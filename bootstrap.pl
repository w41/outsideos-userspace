use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;
use Cwd;

use File::Basename;
use File::Spec;
use lib File::Spec->catdir(
	File::Basename::dirname(File::Spec->rel2abs($0)),
	'perl-modules',
	'lib',
	'perl5');

use Data::Dumper;
use Carp;
use version;
use Graph;
use Config::IniFiles;

=pod

=head1 NAME

bootstrap.pl - build and install outsideos userspace programs

=head1 SYNOPSIS

bootstrap.pl [options] [package[=version] ...]

=head1 OPTIONS

=over 8

=item B<--src-dir=dir>

Set source directory with packages descriptions and patches. Default
is $PWD.

=item B<--build-dir=dir>

Set build directory. Default is $PWD/build.

=item B<--install-dir=dir>

Set installation directory. Default is $PWD/install.

=item B<--jobs=N>

Set number of worker threads. Passed to underlying tools if they
support this. Default is 8.

=item B<--help>

Print help and exit.

=item B<package[=version]>

Package to build in format "package[=version]". May be specified
multiple times. If empty, then everything is built.

=back

=cut

my ($src_dir, $build_dir, $install_dir, $num_jobs, $help);

GetOptions ("src-dir=s" => \$src_dir,
	    "build-dir=s" => \$build_dir,
	    "install-dir=s" => \$install_dir,
	    "jobs=i" => \$num_jobs,
	    "help" => \$help) or pod2usage(2);
pod2usage(1) if $help;

# Normalize path in argument. If argument is defined, then return
# resolved path. If not defined, return default cwd with provided
# subtree.
sub fix_path_arg($@)
{
	my $arg = shift @_;
	$arg = Cwd::realpath($arg) if $arg;
	return $arg || File::Spec->catdir(getcwd, @_)
}

$src_dir = fix_path_arg($src_dir);
$build_dir = fix_path_arg($build_dir, "build");
$install_dir = fix_path_arg($install_dir, "install");

die("Number of workers must be greater than 0") if defined $num_jobs and $num_jobs < 1;
$num_jobs ||= 8;

print("Searching for source in ${src_dir}\n");
print("Building in ${build_dir}\n");
print("Installing to ${install_dir}\n");

sub read_dependencies($$)
{
}

# Resolve relative source path.
sub src($)
{
	return File::Spec->catdir($src_dir, @_);
}

# Resolve relative binary path.
sub bin($)
{
	return File::Spec->catdir($build_dir, @_);
}

# Run escaped command and die if it failed.
sub chk_run(@)
{
	print("Running '@_'\n");
	system(@_);
	confess("Failed to run cmd: @_\n") if $?;
}

# name -> version, ini file
my %available_packages;

# name -> version
my %requested_packages; 

# "name=version" -> config file
my %config_files;

open my $index, '<', src('packages.index') or die("Cannot find package.index in source directory");
while (<$index>) {
	next if (/^#/);
	my ($name, $version) = split ' ', $_;
	confess("No version specified for package $name.") if (!$version); 
	push @{$available_packages{$name}}, version->parse($version);
}
close $index;

foreach (values %available_packages) {
	@{$_} = sort { $b <=> $a } @{$_};
}

#print Dumper(\%available_packages);

while (@ARGV) {
	$_ = shift @ARGV;
	my ($name, $version) = split '=', $_;
	confess("Package ${name} specified twice, this is not supported.") if ($requested_packages{$name});
	$requested_packages{$name} = version->parse($version);
}

if (!%requested_packages) {
	%requested_packages = map { $_ => $available_packages{$_}[0] } keys %available_packages;
}

#print Dumper(\%requested_packages);

# 0. Validate existence of package directory and of the package.conf+rules files in it.

foreach my $name (keys %available_packages) {
	foreach my $version (@{$available_packages{$name}}) {
		my $pkg_dir = src("packages/$name/$version");
		confess("Package directory $name/$version does not exist.") if (!-d $pkg_dir);
		confess("Package description file $name/$version/package.conf does not exist.") if (!-f "$pkg_dir/package.conf");
		confess("Package build rules do not exist.") if (!-f "$pkg_dir/rules");
		$config_files{"$name=$version"} = Config::IniFiles->new( -file => "$pkg_dir/package.conf", -fallback => "general",
										-allowempty => 1);
	}
}

#print Dumper(\%available_packages);

# 1. Resolve dependencies. Those can be in the form of {name}{op}{version}, where op can be =, <=, <, >=, or >; or just {name}.
#    Where there is a choice between versions of the same package, always pick the biggest satisfying version.
#    This may result in unnecessarily failing builds. FIXME: Find a satisfying assignment when there is one.

# @package_search will be used to construct the graph. We take the first element from the left and unshift its dependencies that are
# still unresolved. Each item is a [depender, dependency] pair, where dependency can also have a version constraint.

my @package_search = map { [ "root", "$_" . ($requested_packages{$_} ? "=$requested_packages{$_}" : "") ] } keys %requested_packages;
#print Dumper(\@package_search);

# %packages_to_install is a map into a [min_v, max_v] pair used to track dependencies to specific package versions and whether
# they could be resolved.

my %packages_to_install;

my $depg = Graph->new;

while ($_ = shift @package_search) {
	#	print Dumper(\@{$_});
	@{$_}[1] =~ m/^([^<>=]*)/;

	my $name = $1;

	print("add_edge: @{$_}[0] -> $1\n");
	$depg->add_edge(@{$_}[0], $1);
	print $depg . "\n";
	if ($depg->has_a_cycle) {
		my @cycle = $depg->find_a_cycle;
		confess("Encountered a cycle in the package dependency graph: " . join(" -> ", @cycle) . " -> $cycle[0]");
	}

	# Upon first encounter of a package name, insert with the widest possible range. 
	if (!$packages_to_install{$name}) {
		$packages_to_install{$name} = [ version->parse($available_packages{$name}->[0]), version->parse($available_packages{$name}->[0]) ];
	}

	# Handle the operators to tighten the version constraints.

	if (@{$_}[1] =~ m/(.*)>=(.*)/) {
		if (version->parse($2) > version->parse($packages_to_install{$name}->[0])) {
			$packages_to_install{$name}->[0] = (grep { $_ >= version->parse($2) } @{$available_packages{$name}})[-1];
			confess("no version satisfying constraint $&") if (!$packages_to_install{$name}->[0]);
		}
	} elsif (@{$_}[1] =~ m/(.*)<=(.*)/) { 
		if (version->parse($2) < version->parse($packages_to_install{$name}->[1])) {
			$packages_to_install{$name}->[1] = (grep { $_ <= version->parse($2) } @{$available_packages{$name}})[0];
			confess("no version satisfying constraint $&") if (!$packages_to_install{$name}->[1]);
		}
	} elsif (@{$_}[1] =~ m/(.*)>(.*)/) { 
		if (version->parse($2) > version->parse($packages_to_install{$name}->[0])) {
			$packages_to_install{$name}->[0] = (grep { $_ > version->parse($2) } @{$available_packages{$name}})[-1];
			confess("no version satisfying constraint $&") if (!$packages_to_install{$name}->[0]);
		}
	} elsif (@{$_}[1] =~ m/(.*)<(.*)/) { 
		print "derp\n";
		if (version->parse($2) < version->parse($packages_to_install{$name}->[1])) {
			$packages_to_install{$name}->[1] = (grep { $_ < version->parse($2) } @{$available_packages{$name}})[0];
			confess("no version satisfying constraint $&") if (!$packages_to_install{$name}->[1]);
		}
	} elsif (@{$_}[1] =~ m/(.*)=(.*)/) { 
		$packages_to_install{$name}->[0] = $packages_to_install{$name}->[1] = version->parse($2);
		confess("no version satisfying constraint $&") if (!$packages_to_install{$name}->[1]);
	}

	if ($packages_to_install{$name}->[0] > $packages_to_install{$name}->[1]) {
		confess("Version constraint broken for package $name: $packages_to_install{$name}->[0] > $packages_to_install{$name}->[1]");
	}

	#	print Dumper(\%packages_to_install);

	# FIXME: Again, working with the highest version number available for the package, for now.
	open my $descfile, '<', src("packages/$name/$packages_to_install{$name}->[1]/package.conf");
	while (<$descfile>) {
		if (/^deps=(.*)/) {
			chomp;
			unshift @package_search, map { [ $name, $_ ] } split ' ', $1;
			last;
		}
	}
	close $descfile;
}

#print Dumper(\%packages_to_install);

### 2. Ok, %packages_to_install now contains names and versions in sorted order. Proceed with installation.

foreach (keys %packages_to_install) {

	# Download tarballs if necessary, and extract them into the src directory.
	my $name = $_;
	my $version = $packages_to_install{$_}->[1];
	my @tarballs = split ' ', $config_files{"$_=$version"}->val('general', 'tarballs', '');

	my $tarball_dir = bin("tarballs");
	chk_run("mkdir", "-p", $tarball_dir);
	my $pkg_src_dir = bin("packages/$name/$version/src");
	chk_run("mkdir", "-p", $pkg_src_dir);

	foreach (@tarballs) {
		my $basename = (split('/', $_))[-1];
		if (!-f "$tarball_dir/$basename") {
			chk_run("wget", "-P", $tarball_dir, $_);
		} else {
			print "$basename found in tarball store...\n";
		}
		if ($basename =~ "tar\.gz") {
			chk_run("tar", "-zxvf", "$tarball_dir/$basename", "-C", $pkg_src_dir, "--strip-components=1");
		} elsif ($basename =~ "tar\.xz") {
			chk_run("tar", "-xvf", "$tarball_dir/$basename", "-C", $pkg_src_dir, "--strip-components=1");
		} else {
			confess("Unsupported tarball format: $basename");
		}
	}

	# Apply patches.
	my @patches = glob(src("packages/$name/$version/patches/*.patch"));
	foreach (@patches) {
		print "Applying $_...\n";
		chk_run("patch", "-d", $pkg_src_dir, "-s", "-p0", "-i", $_);
	}

	# Create build directory and run build rules.
	my $pkg_bin_dir = bin("packages/$name/$version/build");
	chk_run("mkdir", "-p", $pkg_bin_dir);
	chdir($pkg_bin_dir);

	my $rules = src("packages/$name/$version/rules");
	# Configuration variables for build rules.
	my @envs = ("HOST=i686-outsideos",
		    "CC=i686-outsideos-gcc",
		    "JOBS=${num_jobs}",
		    "SRC_DIR=${pkg_src_dir}",
		    "BUILD_DIR=${pkg_bin_dir}",
		    # Use this name instead of plain INSTALL_DIR
		    # because of sysvinit make that tries to search
		    # "install" command in this directory...
		    "INSTALL_ROOT_DIR=${install_dir}");
	my @stages = ("configure",
		      "build",
		      "install");
	for (@stages) {
		chk_run("make", "-f", $rules, $_, @envs);
	}
}
